<?php

namespace DKM\BackendPasswordReset\Hooks;

use DKM\BackendPasswordReset\Utility;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class BackendControllerHook
{
    public function passwordResetModal(): void
    {
        if(Utility::getBackendUser()->user['tx_bepwreset_demandReset'] ?? false) {
            $userSetupRoute = (string)GeneralUtility::makeInstance(UriBuilder::class)->buildUriFromRoute('user_PasswordReset');
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class)->addJsInlineCode('tx_bepwreset_demandReset', "
setTimeout(function() {
    require([
      'TYPO3/CMS/Backend/Modal'
    ], function(Modal) {
        const configuration = {
          type: Modal.types.iframe,
          title: 'Vælg venligst et nyt kodeord',
          content: '$userSetupRoute',
          size: Modal.sizes.full,
        };
        Modal.advanced(configuration);
})
},1000);

function openPasswordChangedModal() {
            TYPO3.Modal.confirm('Kodeord skiftet', 'Du har nu valgt et nyt kodeord.', parent.TYPO3.Severity.info, [
        {
                text: 'OK!',
                trigger: function() {
                    TYPO3.Modal.dismiss();
                }
        }
]);
}
    ");
        }
    }
}