<?php

namespace DKM\BackendPasswordReset\Command;

use DKM\BackendPasswordReset\Utility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DemandPasswordReset extends \Symfony\Component\Console\Command\Command
{
    protected function configure()
    {
        $this->setHelp('Set demand value on user');
        $this->addArgument('userType',InputArgument::OPTIONAL, "all, normal, admin");
        $this->addOption('deactivate','d', InputOption::VALUE_NONE, 'Deactivate demand on user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('userType')) {
            case 'all':
                $identifier = ['disable' => 0];
                break;
            case 'admin':
                $identifier = ['admin' => 1];
                break;
            case 'normal':
                $identifier = ['admin' => 0];
                break;
            default:
                return Command::INVALID;
        }
        Utility::setDemandResetOnBackendUsers($identifier, $input->getOption('deactivate'));
        return Command::SUCCESS;
    }



}