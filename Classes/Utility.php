<?php

namespace DKM\BackendPasswordReset;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Utility
{
    static function setDemandResetOnBackendUsers($identifier, $deactivate = false): void
    {
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('be_users');
        $connection->update('be_users', ['tx_bepwreset_demandReset' => $deactivate ? 0 : 1], $identifier);
    }

    /**
     * @return BackendUserAuthentication
     */
    static function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

}