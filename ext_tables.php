<?php

use DKM\BackendPasswordReset\Controller\SetupModuleController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

ExtensionManagementUtility::addModule(
    'user',
    'PasswordReset',
    '',
    null,
    [
        'routeTarget' => SetupModuleController::class . '::mainAction',
//        'access' => 'group,user',
        'name' => 'user_PasswordReset',
        'iconIdentifier' => 'module-setup',
        'labels' => 'LLL:EXT:bepwreset/Resources/Private/Language/locallang_mod.xlf',
    ]
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('options.hideModules.user = PasswordReset');