<?php

/* * *************************************************************
 * Extension Manager/Repository config file for ext "cdsrc_bepwreset".
 *
 * Auto generated 01-02-2015 10:24
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 * ************************************************************* */

$EM_CONF[$_EXTKEY] = [
    'title' => 'BE User Password Reset',
    'description' => 'Remind backend user to reset his password from a modal box. Option can be specified to force Backend user to change his password at first login.',
    'category' => 'Backend',
    'version' => '1.0.0',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => false,
    'author' => 'Stig Nørgaard Færch',
    'author_email' => 'snf@dkm.dk',
    'author_company' => 'Danmarks Kirkelige Mediecenter',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

