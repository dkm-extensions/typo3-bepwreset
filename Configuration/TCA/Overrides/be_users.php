<?php

defined('TYPO3') or die('Access denied.');

/**
 * Add extra field to force password reset at next login
 */
$newBeUsersColumns = array(
    'tx_bepwreset_demandReset' => array(
        'label' => 'Demand password reset',
        'config' => array(
            'type' => 'check',
            'default' => 0
        )
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('be_users', $newBeUsersColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('be_users', 'tx_bepwreset_demandReset;;;;1-1-1','', 'after:password');

